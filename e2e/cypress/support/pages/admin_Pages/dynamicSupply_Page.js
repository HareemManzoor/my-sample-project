import '@testing-library/cypress/add-commands';

export class dynamicSupply_Page{

  selectDeliveryopsTab()
  {
    cy.get('.header-main-container > :nth-child(1) > :nth-child(1) > .anticon > svg',{ timeout: 10000 }).should('be.visible').click()
    cy.get('.ant-menu-root > :nth-child(5) > .ant-menu-submenu-title',{ timeout: 10000 }).should('be.visible').click()
    cy.findAllByText('Dynamic Supply').click() 
  }

  shiftTitle()
  {  
    cy.visit('https://stagingadminportal.grocery.rideairlift.com/dynamic-supply/add-shift')
    cy.get('.ant-form > :nth-child(1) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').type('Test')
  }

  selectCalender()
  {
    cy.wait(5000)
    var today = new Date()
    var dd = today.getDate()
    var mm = today.getMonth()+1
    var yyyy = today.getFullYear()

    if(dd<10) 
    {
      dd='0'+dd
    }  
    if(mm<10) 
    {
      mm='0'+mm
    }
    today = yyyy+'-'+mm+'-'+dd
    console.log(today)
    cy.get('[nz-picker=""] > :nth-child(1) > .ng-tns-c189-36').type(today)
    cy.get('[nz-picker=""] > :nth-child(1) > .ng-tns-c189-36').type('{enter}')
    cy.get(':nth-child(3) > .ng-tns-c189-36').type(today) // calender
    cy.get(':nth-child(3) > .ng-tns-c189-36').type('{enter}')
  }

  selectStartAndEndTime()
  {
    // Select start time
    cy.get(':nth-child(1) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input > .ant-picker-input > .ng-untouched').click()
    cy.get(':nth-child(1) > :nth-child(1) > .ant-picker-time-panel-cell-inner').click()
    cy.get(':nth-child(2) > :nth-child(1) > .ant-picker-time-panel-cell-inner').click()
    cy.get(':nth-child(3) > :nth-child(1) > .ant-picker-time-panel-cell-inner').click()
    cy.get('.cdk-overlay-backdrop').click({force: true})
    cy.get(':nth-child(4) > :nth-child(1) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').click( {force: true} ).type("505")
    // Select end time
    cy.get(':nth-child(2) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input > .ant-picker-input > .ng-pristine',{ timeout: 10000 }).click()
    cy.get(':nth-child(1) > :nth-child(1) > .ant-picker-time-panel-cell-inner').click({ multiple: true })
    cy.get(':nth-child(2) > :nth-child(1) > .ant-picker-time-panel-cell-inner').click({ multiple: true })
    cy.get(':nth-child(3) > :nth-child(1) > .ant-picker-time-panel-cell-inner').click({ multiple: true })
    cy.get('.cdk-overlay-backdrop').click({force: true})
  }

  riderCapacity()
  {
    cy.get(':nth-child(4) > :nth-child(2) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').type('100')
  }

  earlyEnrolmentBonus()
  {
    cy.get('[style="width: 98%; background: lightgray; margin-right: 10px;"] > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').click()
    cy.get('.ant-row.ng-star-inserted > :nth-child(1) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').type("1")
    cy.get('.ant-row.ng-star-inserted > :nth-child(2) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').type("1")
  }

  shiftDuration()
  {
    cy.get(':nth-child(6) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').type("1")
  }

  supplyType()
  {
    cy.get('.mapping-input > .ant-select-selector').click()
    cy.get(':nth-child(1) > .ant-select-item-option-content').click()
  }

  minOrder()
  {
    cy.get(':nth-child(8) > .ant-form-item > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .mapping-input').click()
  }

  selectDay()
  {
    cy.get(':nth-child(2) > .ant-checkbox-wrapper > .ant-checkbox > .ant-checkbox-input').click({force: true})
  }

  createShift()
  {
    cy.get('.ant-form-item-control-input-content > .ant-btn').click({force: true})
  }

  verifyShift()
  {
    cy.get('.ant-notification-notice').should('be.visible')
  }
}
