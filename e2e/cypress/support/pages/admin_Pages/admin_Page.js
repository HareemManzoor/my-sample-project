import '@testing-library/cypress/add-commands';
const config = require('../../../fixtures/config.json')

export class admin_Page {

  get userName() {
    return cy.get('.ng-tns-c135-0 > .ant-input' ,{ timeout: 10000 }).should('be.visible').click()    
  }

  get password(){
    return cy.get('.ng-tns-c135-1 > .ant-input')
  }

  Login(){
    cy.get('.login-form-button').click()
  }

  fillUsername() {
    this.userName.click().clear().type(config.adminUser)
  }

  fillPassword(){
    this.password.click().clear().type(config.adminPassword)
  }

  clickButton(){
    cy.get('.login-form-button').click()
  }

  clickHamburger(){
    cy.get('.header-main-container > :nth-child(1) > :nth-child(1) > .anticon > svg').click()
  }

  clickWarehouse()
  {
    cy.get('.ant-select-selector').click()
    cy.get(':nth-child(3) > .ant-select-item-option-content').click()
  }

  clickInventory(){
    cy.findAllByText("Inventory").click()
    cy.findAllByText("Inventory Insight").click()
  }

  clickAdhocCheckin(){
    cy.visit('https://stagingadminportal.grocery.rideairlift.com/inventory/check-in/adhoc')
  }

  searchProduct(){
    cy.get('.ant-input').type('Sabroso Boneless Breast 1 KG').type('{enter}')
  }

  updateInventory(){
    cy.get(':nth-child(1) > .width-100').clear().type('80')
    cy.get(':nth-child(5) > :nth-child(1) > [type="number"]').clear().type('100')
    cy.get('.ant-picker-input > .ng-pristine').type('2022-06-11').type('{enter}')
    cy.get('.ant-table-cell-fix-right-first > .ant-btn').click()
    cy.get('.ant-modal-confirm-btns > .ant-btn-primary').click( )
  }

  verifyInventory(){
    cy.visit('https://stagingadminportal.grocery.rideairlift.com/inventory/dashboard')
    cy.wait(1500)
    cy.get('.ant-input-affix-wrapper > .ant-input').type('Sabroso Boneless Breast 1 KG').type('{enter}')
    cy.wait(5000)
    cy.get('[style="right: 0px; position: sticky;"] > .ant-btn').click({force: true})
    cy.get('.product-card-name').eq(1)
      .then(($item) => {
        expect($item[0]).to.contain.text('ad hoc check in')
      })   
  }

  clickUserManagement(){
    cy.findAllByText("User Management").click({force:true})
  }

  clickAddUser(){
    cy.get('.ant-col-2 > .ant-btn').click()
  }

  userID_Alpha() {
    var text = ""
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

    for (var i = 0; i < 10; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
  }

  phone_Num() {
    var phone_number = ""
    var possiblenum = "0123456789"

    for (var i = 0; i < 10; i++) {
      phone_number += possiblenum.charAt(Math.floor(Math.random() * possiblenum.length))
    }
    return phone_number
  }

  enterUserDetails(){
    const text = this.userID_Alpha()
    cy.get('#firstName').type('James')
    cy.get('#lastName').type('Enrich')
    cy.get('#password').type('airlift')
    cy.get('#checkPassword').type('airlift')
    cy.get('#username').type(text)
    cy.get('input[formcontrolname="phoneNumber"]').type(this.phone_Num())
    cy.get('.ant-form-item-control-input-content > .ng-tns-c135-47 > .ng-tns-c102-48').click()
    cy.wait(1000)
    cy.get('.cdk-virtual-scroll-viewport').scrollTo('bottom')
    cy.get(':nth-child(12) > .ant-select-item-option-content').click()
    cy.get('.cdk-overlay-backdrop').as('backdrop')
    cy.get('@backdrop').click()
    cy.get('.ant-form-item-control-input-content > .ng-tns-c135-51 > .ng-tns-c102-52').click()
    cy.get(':nth-child(6) > .ant-select-item-option-content').click({force: true})
    cy.get('@backdrop').click({force: true})
    cy.get('.ng-tns-c135-53 > .ant-select-selector > .ant-select-selection-search > .ant-select-selection-search-input').click({force: true})
    cy.wait(1000)
    cy.get('.ng-tns-c102-54 > :nth-child(1) > .cdk-virtual-scroll-viewport > .cdk-virtual-scroll-content-wrapper > .ant-select-item > .ant-select-item-option-content').click()
    cy.get('.ant-form-item-control-input-content > .ng-tns-c135-58').click({force: true})
    cy.get('input[placeholder="Search term"]').type(text).type('{enter}')
    cy.wait(1000)
    cy.get('.ant-table-tbody > .ant-table-row > :nth-child(4)').contains(text).should('have.text', text)
  }
  
  selectFilter()
  {
      cy.get('.ant-select-selector',{ timeout: 30000 }).should('be.visible').click()
  }

  selectWarehouse()
  {
       cy.get(':nth-child(3) > .ant-select-item-option-content').click()
  }

   logOut()
   {
    cy.get('.logout-btn').click()
    cy.get('.ant-modal-confirm-btns > .ant-btn-primary').click()
   }
} 