import '@testing-library/cypress/add-commands';
const config = require('../../../fixtures/config.json')
const route = require('../../../fixtures/routes.json')

export class customer_Page {

    get city() {
        return cy.get('.ant-select-selector').eq(0).click()
    }

    get phoneNumber() {
        return cy.get('#tel')
    }

    get password() {
        return cy.get('[type="password"]')
    }

    fillPhoneNumber() {
        this.phoneNumber.click().clear().type(config.username)
    }

    fillPassword() {
        this.password.click().clear().type(config.password)
    }

    clickButton(btnText) {
        cy.get('button').then(() => {
            cy.contains(btnText).click()
        })
    }

    clickUserMenuItem(menu) {
        cy.get('.ant-dropdown-menu-item').contains(menu).click({ force: true })
    }

    searchArea(area) {
        cy.get('.ant-select-selector').eq(1).click().clear().type(area)
        cy.findAllByText(area).eq(0).click()
    }

    selectCity(city) {
        if (city == "khi") {
            cy.findAllByText("Karachi").eq(1).click()
        }

        else if (city == "lhr") {
            cy.findAllByText("Lahore").eq(1).click()
        }

        else if (city == "isl" || city == "raw") {
            cy.findAllByText(" Islamabad / Rawalpindi").eq(1).click()
        }
    }

    selectUserMenu() {
        cy.get('.user-account-menu').click()
    }

    login() {
        this.fillPhoneNumber()
        this.fillPassword()
        cy.get('.mw-submit-btn').click()
        cy.wait(1000)
        cy.request({
            method: 'POST',
            url: config.apiUrl + route.login,
            headers: {
                'content-type': 'application/json',
            },
            body: {
                username: config.username,
                password: config.password
            }
        })
            .then((resp) => {
                expect(resp.status).to.eq(201)
                expect(resp.body.auth_token).to.exist
            })
    }

    logout() {
        this.selectUserMenu()
        cy.get('.uam-dropdown > :nth-child(6) > span').click()
    }

    verifyCategory() {
        cy.get('.products-nav')
            .eq(1)
            .find('ul > li')
            .should('have.length', 19)
            .then(($item) => {
                expect($item[0]).to.contain.text('Breakfast & Bakery')
                expect($item[1]).to.contain.text('Beverages')
                expect($item[2]).to.contain.text('Frozen')
                expect($item[3]).to.contain.text('Health & Beauty')
                expect($item[4]).to.contain.text('Mother & Baby')
                expect($item[5]).to.contain.text('Dairy & Eggs')
                expect($item[6]).to.contain.text('Household Supplies')
                expect($item[7]).to.contain.text('Cooking Essentials')
                expect($item[8]).to.contain.text('Snacks, Sweets & Chocolates')
                expect($item[9]).to.contain.text('Meat (Frozen)')
                expect($item[10]).to.contain.text('Pet Care')
            })
    }

    verifyComplaintOption() {
        cy.get('.ant-dropdown').should('not.contain', 'Complaints')
    }

    verifyComplaintModal() {
        cy.get('.mw-title').should('contain', 'Complaint')

    }

    verifySubCategory() {
        cy.get('ecp-category > .products-nav')
            .within(() => {
                cy.get('ul > li').within(() => {
                    cy.contains('Breakfast & Bakery').click()
                    cy.get('#chip-cereals-oatmeals > .ft-meta > .ft-label').should('contain', 'Cereals & Oatmeals')
                })
            })
    }
}
