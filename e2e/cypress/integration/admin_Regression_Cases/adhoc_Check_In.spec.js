/// <reference types="Cypress" />
const config = require('../../fixtures/config.json');
const { admin_Page } = require('../../support/pages/admin_Pages/admin_Page.js');

describe('Admin Login Cases', () => {

    const adminPage = new  admin_Page()

    beforeEach(() => {
        cy.visit(config.adminUrl)
        cy.clearLocalStorage()
        adminPage.Login()
        
    })

    it('C868423 - To verify the addhoc check-in and it should reflect on inventory count.', () => {
        
        adminPage.fillUsername()
        adminPage.fillPassword()
        adminPage.clickButton()
        adminPage.clickHamburger()
        adminPage.clickWarehouse()
        adminPage.clickInventory()
        adminPage.clickAdhocCheckin()
        adminPage.searchProduct()
        adminPage.updateInventory()
        adminPage.verifyInventory() 
    })
})
