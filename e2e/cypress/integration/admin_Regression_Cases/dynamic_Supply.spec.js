/// <reference types="Cypress" />
const config = require('../../fixtures/config.json');
const { admin_Page } = require('../../support/pages/admin_Pages/admin_Page.js');
const { dynamicSupply_Page } = require('../../support/pages/admin_Pages/dynamicSupply_Page');

describe('Admin Portal Login',function(){

  const adminPage = new  admin_Page()
  const dynamicSupplyPage = new  dynamicSupply_Page()

  beforeEach(() => {
    cy.visit(config.adminUrl)
    adminPage.fillUsername()
    adminPage.fillPassword()
    adminPage.Login()
    adminPage.selectFilter()
    adminPage.selectWarehouse()
  })

  it('Shift Creation',function(){
    dynamicSupplyPage.selectDeliveryopsTab()
    dynamicSupplyPage.shiftTitle()
    dynamicSupplyPage.selectCalender()
    dynamicSupplyPage.selectStartAndEndTime()
    dynamicSupplyPage.riderCapacity()
    dynamicSupplyPage.earlyEnrolmentBonus()
    dynamicSupplyPage.supplyType()
    dynamicSupplyPage.minOrder()
    dynamicSupplyPage.selectDay()
    dynamicSupplyPage.createShift()
    dynamicSupplyPage.verifyShift()
  })

  afterEach(() => {
    adminPage.logOut()
  })
})