/// <reference types="Cypress" />
const config = require('../../fixtures/config.json');
const { admin_Page } = require('../../support/pages/admin_Pages/admin_Page.js');

describe('Admin Login Cases', () => {

    const adminPage = new  admin_Page()

    beforeEach(() => {
        cy.visit(config.adminUrl) 
        cy.clearLocalStorage()
    })

    it('C838376 - Verify the user management tab.', () => {
        
        adminPage.fillUsername()
        adminPage.fillPassword()
        adminPage.Login()
        adminPage.clickHamburger()
        adminPage.clickWarehouse()
        adminPage.clickUserManagement()
        adminPage.clickAddUser()
        adminPage.enterUserDetails()
        
    })
})
