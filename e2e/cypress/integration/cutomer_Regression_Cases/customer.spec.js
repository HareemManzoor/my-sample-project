/// <reference types="Cypress" />
const config = require('../../fixtures/config.json');
const { customer_Page } = require('../../support/pages/customer_Pages/customer_Page.js');

describe('Customer Login Cases', () => {

    const customerPage = new customer_Page()

    beforeEach(() => {
        cy.visit(Cypress.config('baseUrl'))
        customerPage.city
        customerPage.selectCity("khi")
        customerPage.searchArea("Smart Tower")
        customerPage.clickButton("Continue")
    })

    it('C868423 - To verify that user is able to login app successfully.', () => {
        customerPage.selectUserMenu()
        customerPage.clickButton("Login or Signup")
        customerPage.login()
    })

    it('C868424 - To verify that product category should display correctly.  ', () => {
        customerPage.verifyCategory()
    })

    // it('C868425 - To verify user is able to see sub-category in mobile app.', () => {
    //     customerPage.verifySubCategory()   // Id for sub category is there but its not capturing
    // })

    it('C844434 - To verify that the complaints tab should not visible to logout user.', () => {
        customerPage.selectUserMenu()
        customerPage.verifyComplaintOption()
    })

    it('C844436 - To verify that complaint pop-up should appear when the user click on complaint tab from menu.', () => {
        customerPage.selectUserMenu()
        customerPage.clickButton("Login or Signup")
        customerPage.login()
        customerPage.selectUserMenu()
        customerPage.clickUserMenuItem('Complaints')
        customerPage.verifyComplaintModal()
    })
})
