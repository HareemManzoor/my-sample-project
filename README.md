# My Sample Project
This is a demo project.

## Pre-requisite:
1. If you're using npm to install Cypress, it supports [node](https://nodejs.org/en/download/) versions:
    
   > Node.js 12 or 14 and above
2. Install [github](https://git-scm.com/downloads)


## Project Set-Up:

To start with the project:
1. Create an empty folder in the directory.
2. Navigate to the created folder 
```
    cd /your/project/path
```
3. Clone the project by:
```
    git clone https://gitlab.com/HareemManzoor/my-sample-project.git
```
4. Once project is cloned, Navigate to your e2e folder by: 
```
    cd e2e
```
5. Execute command: 
```
    npm install OR npm i
```
> This command installs dependencies listed in package.json

### Installation of cypress from scratch
1. Create an empty folder in the directory.
1. Navigate to your created folder by: 
```
cd /your/project/folder
```
2. Execute command: 
```
npm install cypress --save-dev
```
>This will install Cypress locally as a dev dependency for your project.

3. To install reporting tool:
```
npm install --save-dev mocha
npm install --save-dev mochawesome
npm install mochawesome-merge --save-dev
npm install --save-dev mochawesome-report-generator
npm install cypress-multi-reporters --save-dev

```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
